using System;

namespace recursividad
{
    internal class MathFunction
    {
        public int n, m, x, y;
        public string cadena;

        public void Factorial()
        {
            Console.WriteLine("\n-Factorial- ");
            Console.WriteLine("Ingrese un numero para calcular el factorial: ");
            n = Int32.Parse(Console.ReadLine());

            Func<int, int> fact = null;
            fact = n => (n == 1) ? 1 : n * fact(n - 1);

            Console.WriteLine($"El factorial del numero {n} es: {fact(n)}");
        }

        public void Sumatoria()
        {
            Console.WriteLine("\n-Sumatoria- ");
            Console.WriteLine("Ingrese un numero para calcular la sumatoria: ");
            n = Int32.Parse(Console.ReadLine());

            Func<int, int> sigma = null;
            sigma = n => (n == 1) ? 1 : n + sigma(n - 1);

            Console.WriteLine($"El factorial del numero {n} es: {sigma(n)}.");
        }

        public void Potencia()
        {
            Console.WriteLine("\n-Potencia- ");
            Console.WriteLine("Ingrese un numero para calcular la potencia: ");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese una potencia: ");
            y = Int32.Parse(Console.ReadLine());

            Func<int, int, int> potencia = null;
            potencia = (x, y) => (y == 0) ? 1 : x * potencia(x, y - 1);

            Console.WriteLine($"La potencia del numero {x} elevado a {y} es: {potencia(x, y)}.");
        }

        public void Multiplicar()
        {
            Console.WriteLine("\n-Multiplicacion- ");
            Console.WriteLine("Ingrese el primer numero: ");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el segundo numero: ");
            y = Int32.Parse(Console.ReadLine());

            Func<int, int, int> multiplicacion = null;
            multiplicacion = (x, y) => (x == 0 || y == 0) ? 0 : x + multiplicacion(x, y - 1);

            Console.WriteLine($"La multiplicacion del numero {x} x {y} es: {multiplicacion(x, y)}.");
        }

        public void Dividir()
        {
            Console.WriteLine("\n-Division- ");
            Console.WriteLine("Ingrese el primer numero (dividendo): ");
            m = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el segundo numero (divisor): ");
            n = Int32.Parse(Console.ReadLine());

            Func<int, int, int> division = null;
            division = (m, n) => (m < n) ? 0 : 1 + division(m - n, n);

            Console.WriteLine($"La division del numero {m} entre el numero {n} es: {division(m, n)}.");
        }

        public void CadenaInversa()
        {
            Console.WriteLine("\n-Inversion de cadenas- ");
            Console.WriteLine("Ingrese una cadena: ");
            cadena = Console.ReadLine();

            Func<string, string> reversion = null;
            reversion = (cadena) => (cadena == null || cadena.Length < 1) ? null : cadena[cadena.Length - 1] + reversion(cadena.Substring(0, (cadena.Length - 1)));

            Console.WriteLine($"La cadena reversa es: {reversion(cadena)}.");
        }

        public void PotenciaSumas()
        {
            Console.WriteLine("\n-Potencia- ");
            Console.WriteLine("Ingrese un numero para calcular la potencia: ");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese una potencia: ");
            y = Int32.Parse(Console.ReadLine());

            Func<int, int, int> potenciaMultiplicacion = null;
            potenciaMultiplicacion = (x, y) => (x == 0 || y == 0) ? 0 : x + potenciaMultiplicacion(x, y - 1);

            Func<int, int, int> potenciaSumas = null;
            potenciaSumas = (m, n) => (m == 0 || n == 0) ? 1 : potenciaMultiplicacion(m, potenciaSumas(m, n - 1));

            Console.WriteLine($"La potencia del numero {x} elevado a {y} es: {potenciaSumas(x, y)}.");
        }

        public void Palindromo()
        {
            Console.WriteLine("\n-Palindromo- ");
            Console.WriteLine("Ingrese una cadena: ");
            cadena = Console.ReadLine();

            Func<string, bool> palindromo = null;
            palindromo = (cadena =>
            {
                if (cadena.Length <= 1)
                    return true;
                else
                {
                    if (cadena[0] != cadena[cadena.Length - 1])
                        return false;
                    else
                        return palindromo(cadena.Substring(1, cadena.Length - 2));
                }
            }
            );

            Console.WriteLine((palindromo(cadena) == false) ? $"\nLa cadena: {cadena}, no es palindromo." : $"\nLa cadena: {cadena}, es palindromo.");
        }

        public void Hanoi()
        {
            n = 4;

            Console.WriteLine("\n-Torres de Hanoi-\n");

            Action<int, char, char, char> recursiveTower = null;
            recursiveTower = (n, inicio, siguiente, auxiliar) =>
            {
                if (n == 1)
                {
                    Console.WriteLine($"Moviendo disco 1 de {inicio} hacia {siguiente}");
                    return;
                }

                recursiveTower(n - 1, inicio, auxiliar, siguiente);

                Console.WriteLine($"Moviendo disco {n} de {inicio} hacia {siguiente}");

                recursiveTower(n - 1, auxiliar, siguiente, inicio);
            };

            recursiveTower(n, 'A', 'C', 'B');
        }

        public void CicloFor()
        {
            Console.WriteLine("\n-Ciclo for recursivo- ");
            Console.WriteLine("Ingrese un numero: ");
            n = Int32.Parse(Console.ReadLine());

            Func<int, int, int> recursiveFor = null;
            recursiveFor = (n, total) => 
            { 
                Console.WriteLine($"\n{n}");
                if(n == 0)
                    return total;
                else
                    return recursiveFor(n - 1, (total + (n)));
            };

            Console.WriteLine($"Suma de todos los numeros: {recursiveFor(n, 0)}");
        }

        public void Fibonacci()
        {
            int i = 0;

            Console.WriteLine("\n-Numeros de Fibonacci- ");

            Console.WriteLine("Ingrese un numero limite: ");
            n = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Func<int, int> recursiveFibonacci = null;
            recursiveFibonacci = (n) => (n < 2) ? 1 : recursiveFibonacci(n - 2) + recursiveFibonacci(n - 1);

            Func<int, int, int> recursiveFor = null;
            recursiveFor = (i, n) => 
            {
                Console.WriteLine($"{recursiveFibonacci(i)}");
                if(i == n)
                    return 0;
                else
                    return recursiveFor(i + 1, n);
            };

            Console.WriteLine($"{recursiveFor(i, n)}");
        }
    }
}