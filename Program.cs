﻿using System;

namespace recursividad
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("- Menu -");
            Console.WriteLine("\n1. Factorial");
            Console.WriteLine("2. Sumatoria");
            Console.WriteLine("3. Potencia");
            Console.WriteLine("4. Multiplicacion");
            Console.WriteLine("5. Division");
            Console.WriteLine("6. Potencia con multiplicacion y suma");
            Console.WriteLine("7. Palindromo");
            Console.WriteLine("8. Invertir cadenas");
            Console.WriteLine("9. Torres de Hanoi");
            Console.WriteLine("10. Ciclo for recursivo");
            Console.WriteLine("11. Fibonacci");
            int option = Int32.Parse(Console.ReadLine());

            MathFunction function = new MathFunction();

            switch (option)
            {
                case 1:
                    function.Factorial();
                    break;
                case 2:
                    function.Sumatoria();
                    break;
                case 3:
                    function.Potencia();
                    break;
                case 4:
                    function.Multiplicar();
                    break;
                case 5:
                    function.Dividir();
                    break;
                case 6:
                    function.PotenciaSumas();
                    break;
                case 7:
                    function.Palindromo();
                    break;
                case 8:
                    function.CadenaInversa();
                    break;
                case 9:
                    function.Hanoi();
                    break;
                case 10:
                    function.CicloFor();
                    break;
                case 11:
                    function.Fibonacci();
                    break;
            }
        }
    }
}
